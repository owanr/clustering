import java.io.*;
import java.util.*;
import java.lang.*;
/*
Clustering Part 1, due 2/21
Written by Brynna Mering and Risako Owan
*/
public class Test {
    private static HashMap<Integer, Double> userEditDict = new HashMap<Integer, Double>();
    private static HashMap<Double, List<Integer>> clusterAssignments = new HashMap<Double, List<Integer>>();
    private static HashMap<Double, Double> totals = new HashMap<Double, Double>();
    private static int k;
    private static Double maxEdits = 0.0; //Used in the process for assigning clusters

    private static void readFileAndLogValues() {
        File dataFile = new File("wp_namespace.txt");
        BufferedReader reader = null;
        //Reads data from file and adds them to userEditDict (ID --> logged edit count)
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            reader.readLine(); //Gets rid of headers
            String line = reader.readLine();
            Integer id = 0;
            while (line != null) {
                String[] splitLine = line.split("\t");
                Double mainEdit = Double.parseDouble(splitLine[1]);
                //Updates our maximum number of edits
                if (mainEdit>maxEdits){
                    maxEdits=mainEdit;
                }              
                userEditDict.put(id, Math.log(1+mainEdit)/Math.log(2));
                id++;
                line = reader.readLine();
            }
            maxEdits = Math.log(1+maxEdits)/Math.log(2);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    /*
    ** Chooses k random points between 0 and maxEdits
    */
    public static List<Double> initializeRandomCenters(){
        List<Double> newCenters = new ArrayList<Double>();
        Random rand = new Random(12345);
        System.out.println("what is k"+k);
        for(int i = 0; i < k; i++){
            Double randomCenter = (maxEdits)*rand.nextDouble();
            newCenters.add(randomCenter);
        }
        return newCenters;
    }

    // !!!!!! if this is too slow we can adjust it to run on a subset instead of the whole data set
    public static List<Double> initializeFancyCenters(){
        Double total = 0.0;
        List<Double> newCenters = new ArrayList<Double>();
        
        for(Integer id : userEditDict.keySet()){
            total = total + userEditDict.get(id);
        }
        Double newCenter = total/userEditDict.size();
        newCenters.add(newCenter);

        while(newCenters.size()<k){
            Double nextCenterDist = 0.0;
            Double nextCenter = null;
            for(Integer id : userEditDict.keySet()){
                Double curMinDist = Double.MAX_VALUE;
                for(Double center : newCenters){
                    Double curDist = calcEucDist(center, userEditDict.get(id));
                    if(curDist<curMinDist){
                        curMinDist = curDist;
                    }
                }
                if(nextCenterDist<curMinDist){
                    nextCenterDist = curMinDist;
                    nextCenter = userEditDict.get(id);
                }
            }
            newCenters.add(nextCenter);
        }
        //System.out.println(newCenters);
        return newCenters;
    }

    /*
    **Calculates the euclidean distance squared: (Difference in distance)^2
    */
    public static Double calcEucDist(Double point1, Double point2){
        return Math.pow(point1-point2, 2);
    }

    // public static void splitHightestSSECluster(){
    //     Double maxSSECenter = getMaxSSECenter(calcSSE());
        
    //     List<Integer> clusterToSplit = new ArrayList<Integer>(clusterAssignments.get(maxSSECenter));
    //     clusterAssignments.remove(maxSSECenter);
    //     totals.remove(maxSSECenter);
    //     List<Integer> smallerThanAvg = new ArrayList<Integer>();
    //     List<Integer> biggerThanAvg = new ArrayList<Integer>();

    //     //Calculating mean for the cluster with max SSE
    //     Double avg = 0.0;
    //     for (Integer pointID : clusterToSplit) {
    //         avg += userEditDict.get(pointID);
    //     }
    //     avg = avg/clusterToSplit.size();

    //     //Splitting cluster into two, according to their values
    //     for (Integer pointID : clusterToSplit) {
    //         if (userEditDict.get(pointID) >= avg){
    //             biggerThanAvg.add(pointID);
    //         } else {
    //             smallerThanAvg.add(pointID);
    //         }
    //     }

    //     //Calculates center for bigger half
    //     //Adds to totals and clusterAssignments
    //     Double centerForBig = 0.0;
    //     for (Integer pointID : biggerThanAvg) {
    //         centerForBig += userEditDict.get(pointID);
    //     }
    //     Double biggerTotal = centerForBig;
    //     centerForBig = centerForBig/biggerThanAvg.size();//this doesn't change biggerTotal
    //     totals.put(centerForBig, biggerTotal);
    //     clusterAssignments.put(centerForBig, biggerThanAvg);

    //     //Calculate center for smaller half
    //     //Adds to totals and clusterAssignments
    //     Double centerForSmall = 0.0;
    //     for (Integer pointID : smallerThanAvg) {
    //         centerForSmall += userEditDict.get(pointID);
    //     }
    //     Double smallerTotal = centerForSmall;
    //     centerForSmall = centerForSmall/smallerThanAvg.size();
    //     totals.put(centerForSmall, smallerTotal);
    //     clusterAssignments.put(centerForSmall, smallerThanAvg);
    // }
 
    public static void assignClusters(List<Double> centerLists){
        boolean shifted = true;

        List<Double> newCenters = centerLists;//new ArrayList<Double>(centerLists);

        //While centers have not converged
        while(shifted == true){

            //Creates new dictionary and totals list with empty/0 values
            clusterAssignments.clear();
            totals.clear();
            List<Integer> emptyValue = new ArrayList<Integer>();
            for(Double center : newCenters){
                clusterAssignments.put(center, emptyValue);
                totals.put(center, 0.0);
            }
            newCenters.clear();//Clears newCenters for reuse.
            
            //Calculates closest center for each point and updates total, clusterAssignments
            Double bestDistance;
            Double bestCenter;
            Double totalSSE = 0.0;
            for(Integer id : userEditDict.keySet()){
                bestDistance = Double.MAX_VALUE;
                bestCenter = null;
                for(Double center : clusterAssignments.keySet()){
                    Double dist = calcEucDist(center, userEditDict.get(id));
                    if(dist<bestDistance){
                        bestDistance = dist;
                        bestCenter = center;
                    }
                }
                totalSSE += bestDistance;
                //Updating total of point values assigned to certain center
                Double tempTotal = totals.get(bestCenter);
                //WHY DOES REMOVING FIRST CHANGE VALUES??
                totals.remove(bestCenter);
                totals.put(bestCenter, tempTotal+userEditDict.get(id));
                //Assigning point to a center
                List<Integer> newClusterList = new ArrayList<Integer>(clusterAssignments.get(bestCenter));
                newClusterList.add(id);
                clusterAssignments.remove(bestCenter);
                clusterAssignments.put(bestCenter, newClusterList);
            }
            System.out.println("***totalSSE"+totalSSE);

            // for(Double center : clusterAssignments.keySet()){
            //     System.out.print(clusterAssignments.get(center).size() + ", ");
            // }
            // System.out.println("");

            //Deals with empty clusters
            List<Double> nullCenters = new ArrayList<Double>();
            for(Double center : clusterAssignments.keySet()){
                if (clusterAssignments.get(center).size() == 0) {
                    nullCenters.add(center);
                }
            }
            while (nullCenters.size() > 0) {
                for (Double nullCenter : nullCenters) {
                    clusterAssignments.remove(nullCenter);
                    totals.remove(nullCenter);
                    handleEmptyClusters();
                }
                //Recounts number of nulls
                nullCenters.clear();
                for(Double center : clusterAssignments.keySet()){
                    if (clusterAssignments.get(center).size() == 0) {
                        nullCenters.add(center);
                    }
                }
            }

            //Calculates the new centers by valvulating average of all points assigned to cluster.
            //Checks to see if the clusters have converged
            shifted = false;
            for(Double center : clusterAssignments.keySet()){
                Double newCenter = totals.get(center)/(clusterAssignments.get(center).size());
                newCenters.add(newCenter);
                if(!(newCenter.equals(center))){
                    shifted = true;
                }
            }
        }//end of while-loop

        //Un-log centers after convergence
        List<Double> finalCenters = new ArrayList<Double>();
        for(Double center : clusterAssignments.keySet()){
            Double fc = Math.pow(2,center) - 1;
            finalCenters.add(fc);
        }
    }


    /*
    ** Discarded code?
    */
    // public static HashMap<Double, Double> calcSSE(){
    //     HashMap<Double, Double> centerSSE = new HashMap<Double, Double>();
    //     Double eachCenterSSE;
    //     //Sums up the Euclidean distance squared for point & center pairs in all clusters
    //     for(Double center : clusterAssignments.keySet()){
    //         eachCenterSSE = 0.0;
    //         List<Integer> values = new ArrayList<Integer>(clusterAssignments.get(center));
    //         for(Integer id : values){
    //             eachCenterSSE = eachCenterSSE + calcEucDist(userEditDict.get(id), center);
    //         }
    //         centerSSE.remove(center);
    //         centerSSE.put(center, eachCenterSSE);
    //     }
    //     return centerSSE;
    // }

    /*
    ** Returns the maximum SSE in the dictionary, centerSSE (center --> SSE).
    */
    public static Double getMaxSSECenter(HashMap<Double, Double> centerSSE) {
        Double maxSSE = 0.0;
        Double maxCenter = 0.0;
        for(Double center : clusterAssignments.keySet()){
            Double curSSE = centerSSE.get(center);
            if (curSSE > maxSSE){
                maxSSE = curSSE;
                maxCenter = center;
            }
        }
        return maxCenter;
    }

    //3. Handle empty clusters
    //At the end of each pass through your data, after you have assigned points to the nearest cluster center, 
    //check to see if any clusters are empty. If you have n empty clusters, find the n points in the dataset that 
    //are furthest from their cluster centers. Throw away any empty cluster centers and replace them with these. 
    //Reassign points to their closest centers. Repeat if necessary.

    public static void handleEmptyClusters(){
        //Gets point furthest from cluster center the new center
        HashMap<Double, Integer> distToID = new HashMap<Double, Integer>();
        PriorityQueue<Double> priorityQueue = new PriorityQueue<Double>();
        Double dist = 0.0;
        for (Double center : clusterAssignments.keySet()){
            List<Integer> assignedPoints = clusterAssignments.get(center);
            for (Integer id : assignedPoints){
                Double pointVal = userEditDict.get(id);
                dist = calcEucDist(center, pointVal);
                priorityQueue.add(dist);
                //We don't care about which point
                distToID.put(dist,id);
            }  
        }
        Double[] resultArray = new Double[priorityQueue.size()];
        priorityQueue.toArray(resultArray);
        Arrays.sort(resultArray);
        Double newCenter = resultArray[priorityQueue.size()-1];
        Integer newCenterID = distToID.get(dist);
        Double newCenterValue = userEditDict.get(newCenterID);
        //Removes new center from whatever cluster it belonged to
        for (Double center : clusterAssignments.keySet()){
            List<Integer> curList = clusterAssignments.get(center);
            if (curList.contains(newCenter)){
                curList.remove(newCenter);
                clusterAssignments.remove(center);
                clusterAssignments.put(center, curList);
                Double newValue = totals.get(center) - newCenterValue;
                totals.put(center, newValue);
            }
        }
        //Makes point furthest from cluster center the new center
        List<Integer> initialList = new ArrayList<Integer>();
        initialList.add(newCenterID);
        clusterAssignments.put(newCenter,initialList);
        totals.put(newCenter,newCenterValue);
    }

    //After done clustering, convert the values back from logged ones
    public static void main(String[] args) {
        readFileAndLogValues();

        //Prompts user for program details.
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String reply = "F";//Assume default is F
        try {
            System.out.println("Would you like to randomly choose centers or fancily? (R/F)");
            reply = reader.readLine();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        

        //Chooses random or fancy
        List<Double> initialCenters;
        if (reply.equals("R")) {
            for (int i = 1; i <= 20; i++){
                k = i;
                assignClusters(initializeRandomCenters());
            }
        } else {
            for (int i = 1; i <= 20; i++){
                k = i;
                assignClusters(initializeFancyCenters());
            }
        }
    }
}