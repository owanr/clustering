import java.io.*;
import java.util.*;
import java.lang.*;
/*
Clustering Part 2, due 2/23
Written by Brynna Mering and Risako Owan
*/
public class Clustering2 {
    private static HashMap<Integer, List<Double>> userEditDict = new HashMap<Integer, List<Double>>();
    private static HashMap<List<Double>, List<Integer>> clusterAssignments = new HashMap<List<Double>, List<Integer>>();
    private static HashMap<List<Double>, List<Double>> totals = new HashMap<List<Double>, List<Double>>();
    private static List<List<Double>> finalCenters = new ArrayList<List<Double>>();
    private static int k;
    private static Double maxMainEdits = 0.0; //Used in the process for assigning clusters
    private static Double maxTalkEdits = 0.0;
    private static Double maxUserTalkEdits = 0.0;
    private static Double maxUserEdits = 0.0;

    private static void readFileAndLogValues() {
        File dataFile = new File("wp_namespace.txt");
        BufferedReader reader = null;
        //Reads data from file and adds them to userEditDict (ID --> logged edit count)
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            reader.readLine(); //Gets rid of headers
            String line = reader.readLine();
            Integer id = 0;
            while (line != null) {
                String[] editsLine = line.split("\t");
                Double mainE = Double.parseDouble(editsLine[1]);
                Double talkE = Double.parseDouble(editsLine[2]);
                Double userTalkE = Double.parseDouble(editsLine[3]);
                Double userE = Double.parseDouble(editsLine[4]);
                Double editsSum = Math.pow(mainE,2) + Math.pow(talkE,2) + Math.pow(userTalkE,2) + Math.pow(userE,2);
                Double magnitude = Math.sqrt(editsSum);
                Double mainEdit = mainE/magnitude;
                Double talkEdit = talkE/magnitude;
                Double userTalkEdit = userTalkE/magnitude;
                Double userEdit = userE/magnitude;
                //Updates our maximum number of edits
                if (mainEdit>maxMainEdits){
                    maxMainEdits=mainEdit;
                }
                if (talkEdit>maxTalkEdits){
                    maxTalkEdits=talkEdit;
                }
                if (userTalkEdit>maxUserTalkEdits){
                    maxUserTalkEdits=userTalkEdit;
                }
                if (userEdit>maxUserEdits){
                    maxUserEdits=userEdit;
                }             
                List<Double> eachUserEditsArray = new ArrayList<Double>();
                eachUserEditsArray.add(mainEdit);
                eachUserEditsArray.add(talkEdit);
                eachUserEditsArray.add(userTalkEdit);
                eachUserEditsArray.add(userEdit);
                userEditDict.put(id, eachUserEditsArray);
                id++;
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    /*
    ** Chooses k random points between 0 and maxEdits
    */
    public static List<List<Double>> initializeRandomCenters(){
        List<List<Double>> newCenters = new ArrayList<List<Double>>();
        Random rand = new Random(12345);
        System.out.println("K is: "+k);
        for(int i = 0; i < k; i++){
            Double randomMainCenter = (maxMainEdits)*rand.nextDouble();
            Double randomTalkCenter = (maxTalkEdits)*rand.nextDouble();
            Double randomUserTalkCenter = (maxUserTalkEdits)*rand.nextDouble();
            Double randomUserCenter = (maxUserEdits)*rand.nextDouble();
            List<Double> newRandomCenter = new ArrayList<Double>();
            newRandomCenter.add(randomMainCenter);
            newRandomCenter.add(randomTalkCenter);
            newRandomCenter.add(randomUserTalkCenter);
            newRandomCenter.add(randomUserCenter);
            newCenters.add(newRandomCenter);
        }
        return newCenters;
    }

    //Initializes centers based on the actual average of the points in the data set
    public static List<List<Double>> initializeFancyCenters(){
        Double mainTotal = 0.0;
        Double talkTotal = 0.0;
        Double userTalkTotal = 0.0;
        Double userTotal = 0.0;

        List<List<Double>> newCenters = new ArrayList<List<Double>>();
        System.out.println("K is: "+k);
        for(Integer id : userEditDict.keySet()){
            List<Double> userEdits = new ArrayList<Double>(userEditDict.get(id));
            mainTotal = mainTotal + userEdits.get(0);
            talkTotal = talkTotal + userEdits.get(1);
            userTalkTotal = userTalkTotal + userEdits.get(2);
            userTotal = userTotal + userEdits.get(3);
        }

        List<Double> newCenter = new ArrayList<Double>();
        newCenter.add(mainTotal/userEditDict.size());
        newCenter.add(talkTotal/userEditDict.size());
        newCenter.add(userTalkTotal/userEditDict.size());
        newCenter.add(userTotal/userEditDict.size());
        newCenters.add(newCenter);

        while(newCenters.size()<k){
            Double nextCenterDist = 0.0;
            List<Double> nextCenter = new ArrayList<Double>();
            for(Integer id : userEditDict.keySet()){
                Double curMinDist = Double.MAX_VALUE;
                for(List<Double> center : newCenters){
                    Double curDist = calcEucDist(center, userEditDict.get(id));
                    if(curDist<curMinDist){
                        curMinDist = curDist;
                    }
                }
                if(nextCenterDist<curMinDist){
                    nextCenterDist = curMinDist;
                    nextCenter = userEditDict.get(id);
                }
            }
            newCenters.add(nextCenter);
        }        return newCenters;
    }

    /*
    **Calculates the euclidean distance squared: (Difference in distance)^2
    */
    public static Double calcEucDist(List<Double> point1, List<Double> point2){
        Double eucDist = Math.pow(point1.get(0)-point2.get(0), 2) + Math.pow(point1.get(1)-point2.get(1), 2)
            + Math.pow(point1.get(2)-point2.get(2), 2) + Math.pow(point1.get(3)-point2.get(3), 2);
        return eucDist;
    }
 
    public static void assignClusters(List<List<Double>> centerLists){
        boolean shifted = true;
        List<List<Double>> newCenters = new ArrayList<List<Double>>(centerLists);
        int roundCount = 0;
        //While centers have not converged
        while(shifted == true){
            roundCount++;
            //Creates new dictionary and totals list with empty/0 values
            clusterAssignments.clear();
            totals.clear();
            List<Integer> emptyValue = new ArrayList<Integer>();
            List<Double> zeros = new ArrayList<Double>();
            for (int i = 0; i<4; i++) {
                zeros.add(0.0);
            }
            for(List<Double> center : newCenters){
                clusterAssignments.put(center, emptyValue);
                totals.put(center, zeros);
            }
            newCenters.clear();//Clears newCenters for reuse.
            
            //Calculates closest center for each point and updates total, clusterAssignments
            Double bestDistance;
            List<Double> bestCenter;
            Double totalSSE = 0.0;
            for(Integer id : userEditDict.keySet()){
                bestDistance = Double.MAX_VALUE;
                bestCenter = null;
                for(List<Double> center : clusterAssignments.keySet()){
                    Double dist = calcEucDist(center, userEditDict.get(id));
                    if(dist<bestDistance){
                        bestDistance = dist;
                        bestCenter = center;
                    }
                }
                totalSSE += bestDistance;
                //Updating total of point values assigned to the associated center
                List<Double> tempTotal = new ArrayList<Double>(totals.get(bestCenter));
                totals.remove(bestCenter);
                for (int i = 0; i < 4; i++){
                    Double temp = tempTotal.get(i);
                    temp = temp + (userEditDict.get(id)).get(i);
                    tempTotal.set(i, temp);
                }
                totals.put(bestCenter, tempTotal);
                //Assigning point to a center
                List<Integer> newClusterList = new ArrayList<Integer>(clusterAssignments.get(bestCenter));
                clusterAssignments.remove(bestCenter);
                newClusterList.add(id);
                clusterAssignments.put(bestCenter, newClusterList);
            }

            System.out.println("SSE round "+roundCount+": "+totalSSE);
            if (clusterAssignments.size() == 2) {
                for (List<Double> key : clusterAssignments.keySet()){
                }
            }

            //Deals with empty clusters
            List<List<Double>> nullCenters = new ArrayList<List<Double>>();
            for(List<Double> center : clusterAssignments.keySet()){
                if (clusterAssignments.get(center).size() == 0) {
                    nullCenters.add(center);
                }
            }

            while (nullCenters.size() > 0) {
                for (List<Double> nullCenter : nullCenters) {
                    clusterAssignments.remove(nullCenter);
                    totals.remove(nullCenter);
                    handleEmptyClusters();
                }
                //Recounts number of nulls
                nullCenters.clear();
                for(List<Double> center : clusterAssignments.keySet()){
                    if (clusterAssignments.get(center).size() == 0) {
                        nullCenters.add(center);
                    }
                }
            }

            //Calculates the new centers by calculating average of all points assigned to cluster.
            //Checks to see if the clusters have converged
            shifted = false;
            for(List<Double> center : clusterAssignments.keySet()){
                List<Double> newCenter = new ArrayList<Double>();
                List<Double> totalForCenter = totals.get(center);
                Double numOfAssignedPoints = (double)clusterAssignments.get(center).size();
                for (int i = 0; i < 4; i++){
                    newCenter.add(totalForCenter.get(i)/numOfAssignedPoints);
                }
                newCenters.add(newCenter);
                
                if(!(newCenter.equals(center))) {
                    shifted = true;
                }
            }   
        }
    }

    //Gets point furthest from cluster center the new center
    public static void handleEmptyClusters(){
        
        HashMap<Double, Integer> distToID = new HashMap<Double, Integer>();
        PriorityQueue<Double> priorityQueue = new PriorityQueue<Double>();
        Double dist = 0.0;
        //Puts all distances between center and point into priority queue and distToID
        for (List<Double> center : clusterAssignments.keySet()){
            List<Integer> assignedPoints = new ArrayList<Integer>(clusterAssignments.get(center));
            for (Integer id : assignedPoints){
                List<Double> pointVals = new ArrayList<Double>(userEditDict.get(id));
                dist = calcEucDist(center, pointVals);
                priorityQueue.add(dist);
                //We don't care about which point
                distToID.put(dist,id);
            }  
        }
        Double[] resultArray = new Double[priorityQueue.size()];
        priorityQueue.toArray(resultArray);
        Arrays.sort(resultArray);
        Double newCenterDist = resultArray[priorityQueue.size()-1];
        //Gets id of the point with the largest distance
        Integer newCenterID = distToID.get(resultArray[priorityQueue.size()-1]);
        List<Double> newCenterValue = new ArrayList<Double>(userEditDict.get(newCenterID));
        //Removes new center from whatever cluster it belonged to
        List<Double> centerToChange = new ArrayList<Double>();
        List<Integer> curList = new ArrayList<Integer>();
        for (List<Double> center : clusterAssignments.keySet()){
            curList = clusterAssignments.get(center);
            if (curList.contains(newCenterID)){
                centerToChange = center;
            }
        }
        clusterAssignments.remove(centerToChange);
        curList.remove(newCenterID);
        clusterAssignments.put(centerToChange, curList);
        List<Double> origValues = new ArrayList<Double>(totals.get(centerToChange));
        for (int i = 0; i < 4; i++){
            Double temp = origValues.get(i);
            temp = temp + newCenterValue.get(i);
            origValues.set(i, temp);
        }
        totals.remove(centerToChange);
        totals.put(centerToChange, origValues);
        //Makes point furthest from cluster center the new center
        List<Integer> initialList = new ArrayList<Integer>();
        initialList.add(newCenterID);
        clusterAssignments.put(newCenterValue,initialList);
        totals.put(newCenterValue,newCenterValue);
    }

    //After done clustering, convert the values back from logged ones
    public static void main(String[] args) {
        readFileAndLogValues();

        //Prompts user for program details.
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String reply = "R";//Assume default is F
        try {
            System.out.println("Would you like to randomly choose centers or choose centers methodically? (R/M)");
            reply = reader.readLine();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        //Chooses random or fancy
        if (reply.equals("R")) {
            for (int i = 1; i <= 20; i++){
                k = i;
                assignClusters(initializeRandomCenters());
            }
        } else {
            for (int i = 1; i <= 20; i++){
                k = i;
                assignClusters(initializeFancyCenters());
            }
        }
    }
}