import java.io.*;
import java.util.*;
import java.lang.*;
/*
Clustering Part 2, due 2/23
Written by Brynna Mering and Risako Owan
*/
public class C2 {
    private static HashMap<Integer, Double[]> userEditDict = new HashMap<Integer, Double[]>();
    private static HashMap<Double, List<Integer>> clusterAssignments = new HashMap<Double, List<Integer>>();
    private static HashMap<Double, Double[]> totals = new HashMap<Double, Double[]>();
    private static List<Double[]> finalCenters = new ArrayList<Double[]>();
    private static int k;
    private static Double maxMainEdits = 0.0; //Used in the process for assigning clusters
    private static Double maxTalkEdits = 0.0;
    private static Double maxUserTalkEdits = 0.0;
    private static Double maxUserEdits = 0.0;

    private static void readFileAndLogValues() {
        File dataFile = new File("wp_namespace.txt");
        BufferedReader reader = null;
        //Reads data from file and adds them to userEditDict (ID --> logged edit count)
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            reader.readLine(); //Gets rid of headers
            String line = reader.readLine();
            Integer id = 0;
            while (line != null) {
                String[] editsLine = line.split("\t");
                Double mainE = Double.parseDouble(editsLine[1]);
                Double talkE = Double.parseDouble(editsLine[2]);
                Double usertalkE = Double.parseDouble(editsLine[3]);
                Double userE = Double.parseDouble(editsLine[4]);
                Double editsSum = mainE + talkE + usertalkE + userE;
                Double mainEdit = mainE/editsSum;
                Double talkEdit = talkE/editsSum;
                Double usertalkEdit = usertalkE/editsSum;
                Double userEdit = userE/editsSum;
                //Updates our maximum number of edits
                if (mainEdit>maxMainEdits){
                    maxMainEdits=mainEdit;
                }
                if (talkEdit>maxTalkEdits){
                    maxTalkEdits=talkEdit;
                }
                if (usertalkEdit>maxUserTalkEdits){
                    maxUserTalkEdits=usertalkEdit;
                }
                if (userEdit>maxUserEdits){
                    maxUserEdits=userEdit;
                }             
                Double[] eachUserEditsArray =  new Double[4] {mainEdit,talkEdit,usertalkEdit,userEdit};
                userEditDict.put(id, eachUserEditsArray);
                id++;
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    /*
    ** Chooses k random points between 0 and maxEdits
    */
    public static List<Double[]> initializeRandomCenters(){
        List<Double[]> newCenters = new ArrayList<Double[]>();
        Random rand = new Random(12345);
        System.out.println("K is: "+k);
        for(int i = 0; i < k; i++){
            Double randomMainCenter = (maxMainEdits)*rand.nextDouble();
            Double randomTalkCenter = (maxTalkEdits)*rand.nextDouble();
            Double randomUserTalkCenter = (maxUserTalkEdits)*rand.nextDouble();
            Double randomUserCenter = (maxUserEdits)*rand.nextDouble();
            Double[] newRandomCenter = new Double[] {randomMainCenter,randomTalkCenter,randomUserTalkCenter,randomUserCenter};
            newCenters.add(newRandomCenter);
        }
        return newCenters;
    }

    // Initializes centers based on the actual average of the points in the data set
    // public static List<Double> initializeFancyCenters(){
    //     Double total = 0.0;
    //     List<Double> newCenters = new ArrayList<Double>();
    //     System.out.println("K is: "+k);
    //     for(Integer id : userEditDict.keySet()){
    //         total = total + userEditDict.get(id);
    //     }
    //     Double newCenter = total/userEditDict.size();
    //     newCenters.add(newCenter);

    //     while(newCenters.size()<k){
    //         Double nextCenterDist = 0.0;
    //         Double nextCenter = null;
    //         for(Integer id : userEditDict.keySet()){
    //             Double curMinDist = Double.MAX_VALUE;
    //             for(Double center : newCenters){
    //                 Double curDist = calcEucDist(center, userEditDict.get(id));
    //                 if(curDist<curMinDist){
    //                     curMinDist = curDist;
    //                 }
    //             }
    //             if(nextCenterDist<curMinDist){
    //                 nextCenterDist = curMinDist;
    //                 nextCenter = userEditDict.get(id);
    //             }
    //         }
    //         newCenters.add(nextCenter);
    //     }        return newCenters;
    // }

    /*
    **Calculates the euclidean distance squared: (Difference in distance)^2
    */
    public static Double calcEucDist(Double[] point1, Double[] point2){
        Double eucDist = Math.pow(point1[0]-point2[0], 2) + Math.pow(point1[1]-point2[1], 2)
            + Math.pow(point1[2]-point2[2], 2) + Math.pow(point1[3]-point2[3], 2);
        return eucDist;
    }
 
    public static void assignClusters(List<Double[]> centerLists){
        boolean shifted = true;

        List<Double[]> newCenters = centerLists;//new ArrayList<Double>(centerLists);
        int roundCount = 0;
        //While centers have not converged
        while(shifted == true){
            roundCount++;
            //Creates new dictionary and totals list with empty/0 values
            clusterAssignments.clear();
            totals.clear();
            List<Integer> emptyValue = new ArrayList<Integer>();
            Double[] zeroArray = new Double[] {0.0,0.0,0.0,0.0};
            for(Double center : newCenters){
                clusterAssignments.put(center, emptyValue);
                totals.put(center, zeroArray);
            }
            newCenters.clear();//Clears newCenters for reuse.
            
            //Calculates closest center for each point and updates total, clusterAssignments
            Double bestDistance;
            Double bestCenter;
            Double totalSSE = 0.0;
            for(Integer id : userEditDict.keySet()){
                bestDistance = Double.MAX_VALUE;
                bestCenter = null;
                for(Double center : clusterAssignments.keySet()){
                    Double dist = calcEucDist(center, userEditDict.get(id));
                    if(dist<bestDistance){
                        bestDistance = dist;
                        bestCenter = center;
                    }
                }
                totalSSE += bestDistance;
                //Updating total of point values assigned to certain center
                Double tempTotal = totals.get(bestCenter);
                totals.remove(bestCenter);
                for (int i = 0; i < 4; i++){
                    tempTotal[i] = tempTotal[i] + userEditDict.get(id)[i];
                }
                totals.put(bestCenter, tempTotal);
                //Assigning point to a center
                List<Integer> newClusterList = new ArrayList<Integer>(clusterAssignments.get(bestCenter));
                newClusterList.add(id);
                clusterAssignments.remove(bestCenter);
                clusterAssignments.put(bestCenter, newClusterList);
            }
            System.out.println("SSE round "+roundCount+": "+totalSSE);

            //Deals with empty clusters
            List<Double> nullCenters = new ArrayList<Double>();
            for(Double center : clusterAssignments.keySet()){
                if (clusterAssignments.get(center).size() == 0) {
                    nullCenters.add(center);
                }
            }
            while (nullCenters.size() > 0) {
                for (Double nullCenter : nullCenters) {
                    clusterAssignments.remove(nullCenter);
                    totals.remove(nullCenter);
                    handleEmptyClusters();
                }
                //Recounts number of nulls
                nullCenters.clear();
                for(Double center : clusterAssignments.keySet()){
                    if (clusterAssignments.get(center).size() == 0) {
                        nullCenters.add(center);
                    }
                }
            }

            //Calculates the new centers by valvulating average of all points assigned to cluster.
            //Checks to see if the clusters have converged
            shifted = false;
            for(Double center : clusterAssignments.keySet()){
                Double[] newCenter = new Double[4];
                for (int i = 0; i < 4; i++){
                    newCenter[i] = totals.get(center)[i]/(clusterAssignments.get(center).size());
                }
                newCenters.add(newCenter);
                if(!(newCenter.equals(center))){
                    shifted = true;
                }
            }
        }//end of while-loop
    }

    public static void handleEmptyClusters(){
        //Gets point furthest from cluster center the new center
        HashMap<Double, Integer> distToID = new HashMap<Double, Integer>();
        PriorityQueue<Double> priorityQueue = new PriorityQueue<Double>();
        Double dist = 0.0;
        for (Double center : clusterAssignments.keySet()){
            List<Integer> assignedPoints = clusterAssignments.get(center);
            for (Integer id : assignedPoints){
                Double pointVals = userEditDict.get(id);
                dist = calcEucDist(center, pointVals);
                priorityQueue.add(dist);
                //We don't care about which point
                distToID.put(dist,id);
            }  
        }
        Double[] resultArray = new Double[priorityQueue.size()];
        priorityQueue.toArray(resultArray);
        Arrays.sort(resultArray);
        Double newCenterDist = resultArray[priorityQueue.size()-1];
        Integer newCenterID = distToID.get(newCenterDist);
        Double[] newCenterValue = userEditDict.get(newCenterID);
        //Removes new center from whatever cluster it belonged to
        for (Double center : clusterAssignments.keySet()){
            List<Integer> curList = clusterAssignments.get(center);
            if (curList.contains(newCenter)){
                curList.remove(newCenter);
                clusterAssignments.remove(center);
                clusterAssignments.put(center, curList);
                Double[] origValues 
                for (int i = 0; i < 4; i++){

                }
                Double newValue = totals.get(center) - newCenterValue;
                totals.put(center, newValue);
            }
        }
        //Makes point furthest from cluster center the new center
        List<Integer> initialList = new ArrayList<Integer>();
        initialList.add(newCenterID);
        clusterAssignments.put(newCenter,initialList);
        totals.put(newCenter,newCenterValue);
    }

    //After done clustering, convert the values back from logged ones
    public static void main(String[] args) {
        readFileAndLogValues();

        //Prompts user for program details.
    //     BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    //     String reply = "F";//Assume default is F
    //     try {
    //         System.out.println("Would you like to randomly choose centers or choose centers methodically? (R/M)");
    //         reply = reader.readLine();
    //         reader.close();
    //     } catch (IOException e) {
    //         e.printStackTrace();
    //     }
        

    //     //Chooses random or fancy
    //     List<Double> initialCenters;
    //     if (reply.equals("R")) {
    //         for (int i = 1; i <= 20; i++){
    //             k = i;
    //             assignClusters(initializeRandomCenters());
    //         }
    //     } else {
    //         for (int i = 1; i <= 20; i++){
    //             k = i;
    //             assignClusters(initializeFancyCenters());
    //         }
    //     }
    }
}